<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = ['name'];

    /**
     * Joins the authors table with 
     * the books table.
     *  
     * 
     */
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
