<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $fillable = ['name'];

    /**
     * Joins the books table
     * with the genres table.
     *
     * 
     */
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
