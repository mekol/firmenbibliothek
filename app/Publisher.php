<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $fillable = ['name'];

    /**
     * Joins the publishers table
     * with the books table.
     *
     * 
     */
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
