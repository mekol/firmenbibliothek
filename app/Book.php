<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title', 'year', 'language_id', 'isbn', 'pages'];

    /**
     * Joins the publishers table
     * with the books table.
     *
     * 
     */
    public function publishers()
    {
        return $this->belongsToMany(Publisher::class);
    }

    /**
     * Joins the authors table
     * with the books table.
     *
     * 
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    /**
     * Joins the genres table
     * with the books table.
     *
     * 
     */
    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    /**
     * Joins the languages table
     * with the books table.
     *
     * 
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * Joins the readers table
     * with the books table.
     *
     * 
     */
    public function readers()
    {
        return $this
            ->belongsToMany(Reader::class, 'book_reader')
            ->using(Checkout::class)
            ->withPivot(['id', 'returndate', 'maxreturndate']);
    }
}
