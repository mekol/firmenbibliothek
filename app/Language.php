<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Language extends Model
{
    protected $fillable = ['name'];

    /**
     * Joins the languages table
     * with the books table.
     *
     * 
     */
    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
