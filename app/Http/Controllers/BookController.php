<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Language;
use App\Genre;
use App\Author;
use App\Publisher;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $books = Book::with(['language', 'authors', 'publishers', 'genres'])->get();

        return view('books/index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        $genres = Genre::all();
        $authors = Author::all();
        $publishers = Publisher::all();

        return view('books/create', compact('languages', 'genres', 'authors', 'publishers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'authors' => 'array|min:1',
            'authors.*' => 'exists:authors,id',
            'year' => 'required|numeric|max:2500',
            'publishers' => 'array|min:1',
            'publishers.*' => 'exists:publishers,id',
            'genres' => 'array|min:1',
            'genres.*' => 'exists:genres,id',
            'language_id' => 'required',
            'isbn' => 'required|numeric|max:9999999999999',
            'pages' => 'required|numeric',
        ]);

        $book = Book::create($validatedData);
        $book->authors()->attach(request('authors'));
        $book->genres()->attach(request('genres'));
        $book->publishers()->attach(request('publishers'));


        return redirect('books')->with('success', 'Buch wurde erfolgreich hinzugefügt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);
        $languages = Language::all();
        $genres = Genre::all();
        $authors = Author::all();
        $publishers = Publisher::all();


        return view('books/show', compact('book', 'languages', 'genres', 'authors', 'publishers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $languages = Language::all();
        $genres = Genre::all();
        $authors = Author::all();
        $publishers = Publisher::all();

        return view('books/edit', compact('book', 'languages', 'genres', 'authors', 'publishers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findorFail($id);
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'authors' => 'exists:authors,id',
            'year' => 'required|numeric|max:2500',
            'publishers' => 'exists:publishers,id',
            'genres' => 'exists:genres,id',
            'language_id' => 'required',
            'isbn' => 'required|numeric|max:9999999999999',
            'pages' => 'required|numeric',
        ]);
        $validatedData = $request->all();
        $book->fill($validatedData)->save();
        $book->authors()->attach(request('authors'));
        $book->genres()->attach(request('genres'));
        $book->publishers()->attach(request('publishers'));

        return redirect('books')->with('success', 'Buch wurde erfolgreich bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $book->delete();

        return redirect('books')->with('success', 'Buch wurde erfolgreich entfernt!');
    }

    /**
     * Searches the resource by the name.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $validatedData = $request->validate([
            'book_search' => 'min:3',
        ]);

        $search = $request->get('book_search');
        $books = Book::with(['language', 'authors', 'publishers', 'genres'])->where('title', 'like', '%' . $search . '%')->get();

        return view('books/index', compact('books'));
    }
}
