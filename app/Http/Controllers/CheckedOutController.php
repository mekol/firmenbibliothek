<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Reader;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CheckedOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('title')->get();
        $readers = Reader::has('books')->get();

        return view('checkedouts/index', compact('books', 'readers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books = Book::doesntHave("readers")->get();
        $readers = Reader::all();

        return view('checkedouts/create', compact('books', 'readers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $reader = Reader::find($request->reader_id);
        $reader
            ->books()
            ->attach(
                $request->book_id,
                ["maxreturndate" => Carbon::now()->addDays(14)]
            );
        $request->validate([
            'book_id' => 'required|exists:books,id',
            'reader_id' => 'required|exists:readers,id'
        ]);

        return redirect('checkedouts')->with('success', 'Buch wurde erfolgreich verliehen!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = DB::table('book_reader')
            ->where('id', $id)
            ->take(1)
            ->delete();

        return redirect('checkedouts')->with('success', 'Das Buch wurde zurückgebracht!');
    }
}
