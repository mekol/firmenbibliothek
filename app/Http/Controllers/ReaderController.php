<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reader;
use Illuminate\Support\Facades\DB;

class ReaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $readers = Reader::all();

        return view('readers/index', compact('readers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('readers/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'employee_number' => 'required|numeric',
        ]);
        $reader = Reader::create($validatedData);

        return redirect('readers')->with('success', 'LeserIn wurde erfolgreich hinzugefügt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reader = Reader::findOrFail($id);

        return view('readers/edit', compact('reader'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'employee_number' => 'required|numeric',
        ]);
        Reader::whereId($id)->update($validatedData);

        return redirect('readers')->with('success', 'LeserIn wurde erfolgreich bearbeitet!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reader = Reader::findOrFail($id);
        $reader->delete();

        return redirect('readers')->with('success', 'LeserIn wurde erfolgreich entfernt!');
    }

    /**
     * Searches the resource by the name.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $validatedData = $request->validate([
            'reader_search' => 'min:3',
        ]);

        $search = $request->get('reader_search');
        $readers = DB::table('readers')
            ->where('name', 'like', '%' . $search . '%')
            ->get();

        return view('readers/index')
            ->with(compact('readers'));
    }
}
