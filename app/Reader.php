<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reader extends Model
{
    protected $fillable = ['name', 'email', 'employee_number'];

    /**
     * Joins the readers table
     * with the books table.
     *
     * 
     */
    public function books()
    {
        return $this
            ->belongsToMany(Book::class, 'book_reader')
            ->using(Checkout::class)
            ->withPivot(['id', 'returndate', 'maxreturndate']);
    }
}
