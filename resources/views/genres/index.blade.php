@extends('layout')


@section('title')
<title>Alle Genres</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
  <div class="alert alert-success">
    {{ session()->get('success') }}
  </div><br />
  @endif
  <div align="left">
    <div class="col-md-4">
      <form action="{{ route('genre_search') }}" method="get" role="search">
        {{ csrf_field() }}
        <div class="input-group">
          <input type="text" class="form-control" name="genre_search" placeholder="Nach Genre suchen..." <span class="input-group-btn">
          <button type="submit" class="btn btn-primary">Suchen</button></span>
          @error('genre_search')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </form>
    </div>
  </div>
  <table class="table table-striped">
    <thead>
      <tr>
        <td>Name</td>
        <td>Funktionen</td>
      </tr>
    </thead>
    <tbody>
      @foreach($genres as $genre)
      <tr>
        <td>{{$genre->name}}</td>
        <td class="d-flex">
          <a href="{{ route('genres.edit', $genre->id)}}"><img src="img/tools.png"></img></a>
          <form action="{{ route('genres.destroy', $genre->id)}}" method="post">
            @csrf
            @method('DELETE')
            <input type="image" src="img/bin.png" name="submit" class="px-2">
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div>
    @endsection