@extends('layout')


@section('title')
<title>LeserIn bearbeiten</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
  LeserIn bearbeiten
  </div>
  <div class="card-body">
    <form method="post" action="{{ route('readers.update', $reader->id) }}">
      
    <div class="form-group">
        @csrf
        @method('PATCH')
        <label for="name">Name: </label>
        <input type="text" class="form-control" name="name" value="{{ $reader->name }}" />
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>

      <div class="form-group">
        @csrf
        <label for="email">E-Mail: </label>
        <input type="email" class="form-control" name="email" value="{{ $reader->email }}" />
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>

      <div class="form-group">
        @csrf
        <label for="employee_number">Mitarbeiternummer: </label>
        <input type="number" class="form-control" name="employee_number" value="{{ $reader->employee_number }}" />
        @error('employee_number')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>

      <button type="submit" class="btn btn-primary">Änderungen speichern</button>
    </form>
  </div>
</div>
@endsection