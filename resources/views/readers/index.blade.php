@extends('layout')


@section('title')
<title>Alle LeserInnen</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <div align="left">
    <div class="col-md-4">
      <form action="{{ route('reader_search') }}" method="get" role="search">
        {{ csrf_field() }}
        <div class="input-group">
          <input type="text" class="form-control" name="reader_search" placeholder="Nach LeserIn suchen..." <span class="input-group-btn">
          <button type="submit" class="btn btn-primary">Suchen</button></span>
          @error('reader_search')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </form>
    </div>
  </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Name</td>
          <td>E-Mail</td>
          <td>Mitarbeiternummer</td>
          <td colspan="2">Funktionen</td>
        </tr>
    </thead>
    <tbody>
        @foreach($readers as $reader)
        <tr>
            <td>{{$reader->name}}</td>
            <td>{{$reader->email}}</td>
            <td>{{$reader->employee_number}}</td>
            
            <td class="d-flex">
              <a href="{{ route('readers.edit', $reader->id)}}"><img src="img/tools.png"></img></a>
                <form action="{{ route('readers.destroy', $reader->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="image" src="img/bin.png" name="submit" class="px-2">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection