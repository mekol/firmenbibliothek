@extends('layout')


@section('title')
<title>Neue(n) LeserIn hinzufügen</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
  Neue(n) LeserIn hinzufügen
  </div>
  <div class="card-body">
    <form method="post" action="{{ route('readers.store') }}">
      <div class="form-group">
        @csrf
        <label for="name">Name: </label>
        <input type="text" class="form-control" name="name" />
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      
      <div class="form-group">
        @csrf
        <label for="email">E-Mail: </label>
        <input type="email" class="form-control" name="email" />
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>

      <div class="form-group">
        @csrf
        <label for="employee_number">Mitarbeiternummer: </label>
        <input type="number" class="form-control" name="employee_number" />
        @error('employee_number')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary">Hinzufügen</button>
    </form>
  </div>
</div>
@endsection