@extends('layout')


@section('title')
<title>Alle Bücher</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
  <div class="alert alert-success">
    {{ session()->get('success') }}
  </div><br />
  @endif
  <div align="left">
    <div class="col-md-4">
      <form action="{{ route('book_search') }}" method="get" role="search">
        {{ csrf_field() }}
        <div class="input-group">
          <input type="text" class="form-control" name="book_search" placeholder="Nach Titel suchen..." <span class="input-group-btn">
          <button type="submit" class="btn btn-primary">Suchen</button></span>
          @error('book_search')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </form>
    </div>
  </div>
  <table class="table table-hover">
    <thead>
      <tr>
        <td>Titel</td>
        <td>Autor</td>
        <td>Jahr</td>
        <td>Genre</td>
        <td>Sprache</td>
        <td>Funktionen</td>
      </tr>
    </thead>
    <tbody>
      @foreach($books as $book)
      <tr>
        <td>{{$book->title}}</td>
        @foreach($book->authors as $author)
        <td>{{$author->name}}</td>
        @endforeach
        <td>{{$book->year}}</td>
        @foreach($book->genres as $genre)
        <td >{{$genre->name}}</td>
        @endforeach
        <td>{{$book->language->name}}</td>

        <td class="d-flex">
          <a href="{{ route('books.edit', $book->id)}}"><img src="img/tools.png"></img></a>
        <a href="{{ route('books.show', $book->id)}}" class="px-2"><img src="img/view.png"></img></a>
          <form action="{{ route('books.destroy', $book->id)}}" method="post">
            @csrf
            @method('DELETE')
            <input type="image" src="img/bin.png" name="submit">
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div>
    @endsection