@extends('layout')

@section('title')
<title>Buch bearbeiten</title>
@section('stylesheets')
<script src="http://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />


@endsection
@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>

<div class="card uper">
    <div class="card-header">
        Buch bearbeiten
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('books.update', $book->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="title">Titel:</label>
                <input type="text" class="form-control" name="title" value="{{ $book->title }}" />
                @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            
            <div class="form-group">
                <label for="authors">Author(en):</label>
                <select name="authors[]" multiple class="form-control select2-multi <!-- @error('authors') is-invalid @enderror -->">
                    @foreach ($authors as $author)
                    <option value="{{ $author->id }}" {{ in_array($author->id, old('authors') ?? []) ? 'selected' : '' }}>
                        {{ $author->name }}
                    </option>
                    @endforeach
                </select>
                @error('authors')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="year">Jahr:</label>
                <input type="text" class="form-control" name="year" value="{{ $book->year }}" />
                @error('year')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="publishers">Verlag(e):</label>
                <select name="publishers[]" multiple class="form-control select2-multi <!-- @error('publishers') is-invalid @enderror -->">
                    @foreach ($publishers as $publisher)
                    <option value="{{ $publisher->id }}" {{ in_array($publisher->id, old('publishers') ?? []) ? 'selected' : '' }}>
                        {{ $publisher->name }}
                    </option>
                    @endforeach
                </select>
                @error('publishers')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="genres">Genre(s):
                    @foreach($genres as $genre)
                    <input type="checkbox" name="genres[{{$genre->id}}]" @if(array_key_exists($genre->id, old('genres', []))) checked @endif value="{{ $genre->id }}" > {{$genre->name}}
                    @endforeach
                </label>
                @error('genre.'. $genre->id)
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            </div>

            <div class="form-group">
                <label for="language_id">Sprache:</label>
                <select name="language_id" class="form-control @error('language_id') is-invalid @enderror">
                    <option value="{{ old('language_id')}}">-- {{ __('Spache auswählen') }} --</option>
                    @foreach ($languages as $language)
                    @if (old('language_id') == $language->id)
                    <option value="{{ $language->id }}" selected>{{ $language->name }}</option>
                    @else
                    <option value="{{ $language->id }}">{{ $language->name }}</option>
                    @endif
                    @endforeach
                </select>
                @error('language_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="isbn">ISBN:</label>
                <input type="text" class="form-control" name="isbn" value="{{ $book->isbn }}" />
                @error('isbn')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="pages">Seitenzahl:</label>
                <input type="text" class="form-control" name="pages" value="{{ $book->pages }}" />
                @error('pages')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Hinzufügen</button>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(".select2-multi").select2();
</script>
@endsection