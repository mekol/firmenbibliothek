@extends('layout')

@section('title')
<title>Autor bearbeiten</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Autor bearbeiten
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('authors.update', $author->id) }}">
      <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="name">Titel:</label>
              <input type="text" class="form-control" name="name" value="{{ $author->name }}"/>
          </div>
          <button type="submit" class="btn btn-primary">Änderungen speichern</button>
      </form>
  </div>
</div>
@endsection