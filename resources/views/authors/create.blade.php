@extends('layout')


@section('title')
<title>Neuen Autor hinzufügen</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Neuen Autor hinzufügen
  </div>
  <div class="card-body">
    <form method="post" action="{{ route('authors.store') }}">
      <div class="form-group">
        @csrf
        <label for="name">Name: </label>
        <input type="text" class="form-control" name="name" />
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
  </div>
  <button type="submit" class="btn btn-primary">Hinzufügen</button>
  </form>
</div>
</div>
@endsection