@extends('layout')


@section('title')
<title>Home</title>
@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="uper">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div style="align-content: center"><h1>Willkommen zurück!</h1>
                    <p>Sie haben sich erfolgreich als Admin angemeldet!</p>
                    <img src="../img/library.png" alt="" >
                    <img src="../img/library2.png" alt="" >
                    <img src="../img/library3.png" alt="" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection