@extends('layout')


@section('title')
<title>Alle Verlage</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <div align="left">
    <div class="col-md-4">
      <form action="{{ route('publisher_search') }}" method="get" role="search">
        {{ csrf_field() }}
        <div class="input-group">
          <input type="text" class="form-control" name="publisher_search" placeholder="Nach Verlag suchen..." <span class="input-group-btn">
          <button type="submit" class="btn btn-primary">Suchen</button></span>
          @error('publisher_search')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </form>
    </div>
  </div>
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Name</td>
          <td colspan="2">Funktionen</td>
        </tr>
    </thead>
    <tbody>
        @foreach($publishers as $publisher)
        <tr>
            <td>{{$publisher->name}}</td> 
            <td class="d-flex">
              <a href="{{ route('publishers.edit', $publisher->id)}}"><img src="img/tools.png"></a>
                <form action="{{ route('publishers.destroy', $publisher->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="image" src="img/bin.png" name="submit" class="px-2">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection