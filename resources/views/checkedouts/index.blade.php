@extends('layout')


@section('title')
<title>Alle ausgeliehen Bücher</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
  <div class="alert alert-success">
    {{ session()->get('success') }}
  </div><br />
  @endif

  <table class="table table-hover">
    <thead>
      <tr>
        <td>Verliehen an</td>
        <td>Titel</td>
        <td>Verliehen bis</td>
        <td>Bearbeiten</td>

      </tr>
    </thead>
    <tbody>
    @foreach($readers as $reader)
        <tr>
            <td rowspan="{{ $reader->books->count() }}">{{$reader->name}}</td>
            @foreach($reader->books as $book)
                {!! $loop->first ? '' : '</tr><tr>' !!}

                <td>{{$book->title}}</td>
                <td>{{$book->pivot->maxreturndate}}</td>
                <td>
                    <form action="{{ route('checkedouts.destroy', $book->pivot->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Buch zurückgebracht</button>
                    </form>
                </td>

                {!! $loop->last ? '' : '</tr>' !!}
            @endforeach
        </tr>
    @endforeach
</tbody>
  </table>
  <div>
    @endsection