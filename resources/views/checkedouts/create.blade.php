@extends('layout')


@section('title')
<title>Buch verleihen</title>
@section('stylesheets')
<script src="http://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />


@endsection
@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="card uper">
    <div class="card-header">
        Buch verleihen
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('checkedouts.store') }}">
            <div class="form-group">
                @csrf
                <label for="book_id">Buch:</label>
                <select name="book_id" class="form-control select2-single <!-- @error('book_id') is-invalid @enderror -->">
                    @foreach ($books as $book)
                    <option value="{{ $book->id }}">{{ $book->title }}</option>
                    @endforeach
                </select>
                @error('book_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="reader_id">Verleihen an:</label>
                <select name="reader_id" class="form-control select2-single <!-- @error('reader_id') is-invalid @enderror -->">
                    @foreach ($readers as $reader)
                    <option value="{{ $reader->id }}">{{ $reader->name }}</option>
                    @endforeach
                </select>
                @error('reader_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Verleihen</button>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(".select2-single").select2();
</script>

@endsection