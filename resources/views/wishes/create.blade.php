@extends('layout')


@section('title')
<title>Neuer Wunsch</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Neuer Wunsch
  </div>
  <div class="card-body">
    <form method="post" action="{{ route('wishes.store') }}">
      <div class="form-group">
        @csrf
        <label for="author">Autor:</label>
        <input type="text" class="form-control" name="author" value="{{ old('author')}}" />
        @error('author')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="title">Titel:</label>
        <input type="text" class="form-control" name="title" value="{{ old('title')}}" />
        @error('title')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary">Versenden</button>
    </form>
  </div>
</div>
@endsection