@extends('layout')


@section('title')
<title>Alle Wünsche</title>
@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Autor</td>
          <td>Titel</td>
          <td>Funktionen</td>
        </tr>
    </thead>
    <tbody>
        @foreach($wishes as $wish)
        <tr>
            <td>{{$wish->id}}</td>
            <td>{{$wish->author}}</td>
            <td>{{$wish->title}}</td>
            
            <td>
                <form action="{{ route('wishes.destroy', $wish->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Löschen</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection