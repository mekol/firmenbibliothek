<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::resource('books', 'BookController')->middleware(['is_admin']);

Route::resource('authors', 'AuthorController')->middleware(['is_admin']);;

Route::resource('languages', 'LanguageController')->middleware(['is_admin']);;

Route::resource('genres', 'GenreController')->middleware(['is_admin']);;
Route::resource('publishers', 'PublisherController')->middleware(['is_admin']);;
Route::resource('wishes', 'WishController')->middleware(['is_admin']);;
Route::resource('readers', 'ReaderController')->middleware(['is_admin']);;
Route::resource('checkedouts', 'CheckedOutController')->middleware(['is_admin']);;



Route::get('/user', function () {
    return view('user');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/register', function () {
    return view('register');
});

Auth::routes();


Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');
Route::get('/home', 'HomeController@index');

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('book_search','BookController@search')->name('book_search')->middleware('is_admin');;
Route::get('author_search','AuthorController@search')->name('author_search')->middleware('is_admin');;
Route::get('genre_search','GenreController@search')->name('genre_search')->middleware('is_admin');;
Route::get('language_search','LanguageController@search')->name('language_search')->middleware('is_admin');;
Route::get('publisher_search','PublisherController@search')->name('publisher_search')->middleware('is_admin');;
Route::get('reader_search','ReaderController@search')->name('reader_search')->middleware('is_admin');;